import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatToolbarModule} from '@angular/material';
import { BodyComponent } from './body/body.component';
import { Body2Component } from './body2/body2.component';
import { Body3Component } from './body3/body3.component';
import { Body4Component } from './body4/body4.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BodyComponent,
    Body2Component,
    Body3Component,
    Body4Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
